#ifndef TASK_2_EXECUTOR_H_
#define TASK_2_EXECUTOR_H_

#include "Blocks_Factory/Block_Factory.h"
#include "Parser.h"

class Executor {
private:
    std::map <unsigned int, std::shared_ptr<Worker>> block_map;
public:
    void execute (const std::string&);
};

#endif
