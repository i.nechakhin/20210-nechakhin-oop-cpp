#include "Executor.h"

void Executor::execute (const std::string& file_name) {
    try {
        vector_pair blocks;
        std::vector <int> queue;
        Parser parser;
        parser.parse(file_name, &blocks, &queue);
        Block_Factory& block_factory = Block_Factory::get_instance_factory();
        size_t size_blocks = blocks.size();
        for (size_t i = 0; i < size_blocks; ++ i) {
            std::shared_ptr <Worker> block = Block_Factory::create_block(blocks[i].second.first, blocks[i].second.second);
            if (block == nullptr) {
                throw File_Not_Contain(file_name, "correct block");
            }
            block_map[blocks[i].first] = block;
        }
        auto* text = new std::vector <std::string>();
        size_t size_queue = queue.size();
        for (size_t i = 0; i < size_queue; ++i) {
            if (i == 0 && block_map[queue[i]]->get_type() != BLOCK_TYPE::IN) {
                throw Queue_Error(0);
            }
            if (0 < i && i < (size_queue - 1) && block_map[queue[i]]->get_type() != BLOCK_TYPE::INOUT) {
                throw Queue_Error(1);
            }
            if (i == (size_queue - 1) && block_map[queue[i]]->get_type() != BLOCK_TYPE::OUT) {
                throw Queue_Error(2);
            }
            block_map[queue[i]]->work(text);
        }
        delete text;
    }
    catch (File_Not_Contain& error) {
        std::cout << error.get_error();
    }
    catch (File_Not_Opened& error) {
        std::cout << error.get_error();
    }
    catch (File_Error& error) {
        std::cout << error.get_error();
    }
    catch (Queue_Error& error) {
        std::cout << error.get_error();
    }
    catch (Text_Empty& error) {
        std::cout << error.get_error();
    }
    catch (Text_Error& error) {
        std::cout << error.get_error();
    }
    catch (Parameter_Empty& error) {
        std::cout << error.get_error();
    }
    catch (Parameter_Error& error) {
        std::cout << error.get_error();
    }
}
