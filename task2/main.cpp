#include <string>

#include "Executor.h"

int main () {
    Executor executor;
    std::string file_name;
    file_name = "/home/ilya/CLionProjects/20210-nechakhin-oop-cpp/task2/workflows/uncorrect_workflow_1.txt";
    executor.execute(file_name);
    file_name = "/home/ilya/CLionProjects/20210-nechakhin-oop-cpp/task2/workflows/uncorrect_workflow_2.txt";
    executor.execute(file_name);
    file_name = "/home/ilya/CLionProjects/20210-nechakhin-oop-cpp/task2/workflows/uncorrect_workflow_3.txt";
    executor.execute(file_name);
    file_name = "/home/ilya/CLionProjects/20210-nechakhin-oop-cpp/task2/workflows/uncorrect_workflow_4.txt";
    executor.execute(file_name);
    file_name = "/home/ilya/CLionProjects/20210-nechakhin-oop-cpp/task2/workflows/correct_workflow_1.txt";
    executor.execute(file_name);
    file_name = "/home/ilya/CLionProjects/20210-nechakhin-oop-cpp/task2/workflows/correct_workflow_2.txt";
    executor.execute(file_name);
    return 0;
}
