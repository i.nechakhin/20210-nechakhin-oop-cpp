#include "Parser.h"

std::vector <std::string> Parser::split_equal (const std::string& line) {
    std::vector <std::string> vector = { "", "" };
    size_t size_line = line.size();
    bool is_equal = false;
    for (int i = 0; i < size_line; ++ i) {
        if (line[i] == '=') {
            is_equal = true;
            continue;
        }
        if (!is_equal) {
            vector[0].push_back(line[i]);
        } else {
            vector[1].push_back(line[i]);
        }
    }
    if (!is_equal) {
        vector.clear();
    }
    return vector;
}

std::pair <std::string, std::vector <std::string>> Parser::split_space (const std::string& line) {
    std::string block_name;
    std::vector <std::string> params;
    size_t size_line = line.size();
    std::string temp;
    int count_string = 0;
    for (int i = 0; i < size_line; ++ i) {
        if (line[i] == ' ') {
            if (!temp.empty()) {
                if (count_string == 0) {
                    block_name = temp;
                } else {
                    params.push_back(temp);
                }
                temp.clear();
                count_string ++;
            }
        } else {
            temp.push_back(line[i]);
        }
    }
    if (!temp.empty()) {
        if (count_string == 0) {
            block_name = temp;
        } else {
            params.push_back(temp);
        }
    }
    return std::pair <std::string, std::vector <std::string>> (block_name, params);
}

std::vector <int> Parser::split_queue (const std::string& line) {
    std::vector <int> vector;
    size_t size_line = line.size();
    bool is_imp = false;
    std::string temp;
    for (int i = 0; i < size_line; ++ i) {
        if (line[i] == '-' && line[i + 1] == '>') {
            is_imp = true;
            if (!temp.empty()) {
                vector.push_back(std::stoi(temp));
                temp = "";
            }
        } else if (line[i] == ' ' || line[i] == '>') {
            continue;
        } else {
            temp.push_back(line[i]);
        }
    }
    if (!temp.empty()) {
        vector.push_back(std::stoi(temp));
    }
    if (!is_imp) {
        vector.clear();
    }
    return vector;
}

bool Parser::is_file_contain (const std::string& file_name, const std::string& string) {
    std::ifstream file;
    file.open(file_name);
    std::string line;
    bool is_contain = false;
    while (getline(file, line)) {
        if (line == string) {
            is_contain = true;
        }
    }
    file.close();
    return is_contain;
}

void Parser::parse (const std::string& file_name, vector_pair* blocks, std::vector <int>* queue) {
    std::ifstream file;
    file.open(file_name);
    if (!file.is_open()) {
        throw File_Not_Opened(file_name);
    }
    if (!is_file_contain(file_name, "desc")) {
        throw File_Not_Contain(file_name, "desc");
    }
    if (!is_file_contain(file_name, "csed")) {
        throw File_Not_Contain(file_name, "csed");
    }
    std::string line;
    bool is_workflow_block = false;
    bool is_queue_block = false;
    while (getline(file, line)) {
        if (line == "desc") {
            is_workflow_block = true;
            continue;
        }
        if (line == "csed") {
            is_queue_block = true;
            continue;
        }
        if (is_workflow_block && !is_queue_block) {
            std::vector <std::string> num_and_block_info = split_equal(line);
            if (num_and_block_info.empty()) {
                throw File_Not_Contain(file_name, "symbol =");
            }
            int number_block = std::stoi(num_and_block_info[0]);
            std::pair <std::string, std::vector <std::string>> block_and_params = split_space(num_and_block_info[1]);
            blocks->push_back(std::pair <int, std::pair <std::string, std::vector <std::string>>>(number_block, block_and_params));
        }
        if (is_queue_block) {
            *queue = split_queue(line);
            if (queue->empty()) {
                throw File_Not_Contain(file_name, "symbol ->");
            }
            break;
        }
    }
    file.close();
}
