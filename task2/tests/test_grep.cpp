#include <gtest/gtest.h>
#include <iostream>

#include "../Blocks_Factory/Blocks/Grep.h"

::testing::AssertionResult comp_vector (const std::vector <std::string>& vector1, const std::vector <std::string>& vector2) {
    size_t size_vector1 = vector1.size();
    size_t size_vector2 = vector2.size();
    if (size_vector1 != size_vector2) {
        return ::testing::AssertionFailure();
    }
    for (size_t i = 0; i < size_vector2; ++ i) {
        if (vector1[i] != vector2[i]) {
            return ::testing::AssertionFailure();
        }
    }
    return ::testing::AssertionSuccess();
}

TEST (readfile, normal_nose) {
    std::vector <std::string> right_text1 = {"And she's buying a stairway to Heaven",
                                             "Ooh, ooh, and she's buying a stairway to Heaven",
    };
    Grep grep1 = Grep({"stairway"});
    std::vector <std::string> text1 = {"There's a lady who's sure all that glitters is gold",
                                        "And she's buying a stairway to Heaven",
                                        "When she gets there she knows, if the stores are all closed",
                                        "With a word she can get what she came for",
                                        "Ooh, ooh, and she's buying a stairway to Heaven",
                                        "There's a sign on the wall, but she wants to be sure",
                                        "'Cause you know sometimes words have two meanings",
                                        "In a tree by the brook, there's a songbird who sings",
                                        "Sometimes all of our thoughts are misgiven"
    };
    grep1.work(&text1);
    EXPECT_TRUE(comp_vector(right_text1, text1));
    std::vector <std::string> right_text2 = {"And revery.",
                                             "The revery alone will do,",
    };
    Grep grep2 = Grep({"revery"});
    std::vector <std::string> text2 = {"To make a prairie it takes a clover and one bee,",
                                       "One clover, and a bee,",
                                       "And revery.",
                                       "The revery alone will do,",
                                       "If bees are few."
    };
    grep2.work(&text2);
    EXPECT_TRUE(comp_vector(right_text2, text2));
}

TEST (readfile, not_normal_nose) {
    Grep grep1 = Grep({""});
    std::vector <std::string> text1;
    ASSERT_THROW(grep1.work(&text1), Parameter_Error);
    Grep grep2 = Grep({"revery"});
    std::vector <std::string> text2 = {};
    ASSERT_THROW(grep2.work(&text1), Text_Empty);
}

int main (int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}