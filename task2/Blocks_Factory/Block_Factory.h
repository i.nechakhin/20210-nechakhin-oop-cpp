#ifndef TASK_2_BLOCK_FACTORY_H_
#define TASK_2_BLOCK_FACTORY_H_

#include <map>
#include <memory>

#include "Blocks/Dump.h"
#include "Blocks/Grep.h"
#include "Blocks/Readfile.h"
#include "Blocks/Replace.h"
#include "Blocks/Sort.h"
#include "Blocks/Writefile.h"

class Block_Factory {
private:
    Block_Factory () = default;
    ~Block_Factory() = default;
    static std::shared_ptr<Worker> select_and_create_block (std::string block_name, const std::vector <std::string>& params);
    static void do_lower (std::string&);
public:
    Block_Factory (const Block_Factory&) = delete;
    Block_Factory& operator = (const Block_Factory&) = delete;
    static Block_Factory& get_instance_factory ();
    static std::shared_ptr <Worker> create_block (const std::string& block_name, const std::vector <std::string>& params);
};

#endif