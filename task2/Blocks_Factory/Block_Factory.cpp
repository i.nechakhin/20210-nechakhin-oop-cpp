#include "Block_Factory.h"

void Block_Factory::do_lower (std::string& string) {
    size_t size_string = string.size();
    for (size_t i = 0; i < size_string; ++ i) {
        string[i] = std::tolower(string[i]);
    }
}

std::shared_ptr <Worker> Block_Factory::select_and_create_block (std::string block_name, const std::vector <std::string>& params) {
    if (params.empty() && block_name != "sort") {
        throw Parameter_Empty();
    }
    do_lower(block_name);
    std::shared_ptr <Worker> block;
    if (block_name == "readfile") {
        block = std::make_shared <Readfile>(params);
        return block;
    } else if (block_name == "writefile") {
        block = std::make_shared <Writefile>(params);
        return block;
    } else if (block_name == "grep") {
        block = std::make_shared <Grep>(params);
        return block;
    } else if (block_name == "sort") {
        block = std::make_shared <Sort>(params);
        return block;
    } else if (block_name == "replace") {
        block = std::make_shared <Replace>(params);
        return block;
    } else if (block_name == "dump") {
        block = std::make_shared <Dump>(params);
        return block;
    } else {
        return nullptr;
    }
}

Block_Factory& Block_Factory::get_instance_factory () {
    static Block_Factory factory;
    return factory;
}

std::shared_ptr <Worker> Block_Factory::create_block (const std::string& block_name, const std::vector <std::string>& params) {
    auto block = select_and_create_block(block_name, params);
    return block;
}