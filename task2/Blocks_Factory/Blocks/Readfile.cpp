#include "Readfile.h"

Readfile::Readfile (const std::vector <std::string>& input_params) {
    params = input_params;
}

void Readfile::validate (std::vector <std::string>* text) {
    if (params.empty()) {
        throw Parameter_Empty();
    }
    std::ifstream input_file;
    input_file.open(params[0]);
    if (!input_file.is_open()) {
        throw File_Not_Opened(params[0]);
    }
    input_file.close();
}

// params[0] == filename
void Readfile::do_work (std::vector <std::string>* text) {
    std::ifstream input_file;
    input_file.open(params[0]);
    std::string line;
    while (getline(input_file, line)) {
        text->push_back(line);
    }
    input_file.close();
}

BLOCK_TYPE Readfile::get_type () {
    return BLOCK_TYPE::IN;
}