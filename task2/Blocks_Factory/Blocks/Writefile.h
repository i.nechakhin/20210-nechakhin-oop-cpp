#ifndef TASK_2_WRITEFILE_H_
#define TASK_2_WRITEFILE_H_

#include <fstream>

#include "Worker.h"

class Writefile : public Worker {
private:
    std::vector <std::string> params;
public:
    explicit Writefile (const std::vector <std::string>&);
    void validate (std::vector<std::string> *text) override;
    void do_work (std::vector <std::string>* text) override;
    BLOCK_TYPE get_type () override;
};

#endif