#include "Sort.h"

Sort::Sort (const std::vector <std::string>& input_params) {
    params = input_params;
}

void Sort::validate (std::vector <std::string>* text) {
    if (!params.empty()) {
        throw Parameter_Error();
    }
    if (text->empty()) {
        throw Text_Empty();
    }
}

void Sort::do_work (std::vector <std::string>* text) {
    sort((*text).begin(), (*text).end(), std::less <>());
}

BLOCK_TYPE Sort::get_type () {
    return BLOCK_TYPE::INOUT;
}