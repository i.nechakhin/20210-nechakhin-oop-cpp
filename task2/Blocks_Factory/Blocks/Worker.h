#ifndef TASK_2_WORKER_H_
#define TASK_2_WORKER_H_

#include <iostream>
#include <string>
#include <vector>

#include "Validator.h"

enum class BLOCK_TYPE {
    IN,
    OUT,
    INOUT
};

class Worker {
protected:
    virtual void do_work (std::vector <std::string>* text) = 0;
public:
    virtual void validate (std::vector <std::string>* text) = 0;
    void work (std::vector <std::string>* text);
    virtual BLOCK_TYPE get_type () = 0;
};

#endif