#ifndef TASK_2_GREP_H_
#define TASK_2_GREP_H_

#include "Worker.h"

class Grep : public Worker {
private:
    std::vector <std::string> params;
    static bool is_word_in_string (std::string, std::string);
public:
    explicit Grep (const std::vector <std::string>&);
    void validate (std::vector<std::string> *text) override;
    void do_work (std::vector <std::string>* text) override;
    BLOCK_TYPE get_type () override;
};

#endif