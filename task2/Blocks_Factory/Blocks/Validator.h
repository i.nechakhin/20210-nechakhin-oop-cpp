#ifndef TASK_2_VALIDATOR_H_
#define TASK_2_VALIDATOR_H_

#include <iostream>
#include <string>

class Base_Error : std::exception {
public:
    virtual std::string get_error () = 0;
};

class File_Error : public Base_Error {
private:
    std::string text_error;
public:
    File_Error ();
    std::string get_error () override;
};

class File_Not_Opened : public File_Error {
private:
    std::string text_error;
public:
    explicit File_Not_Opened (const std::string&);
    std::string get_error () override;
};

class File_Not_Contain : public File_Error {
private:
    std::string text_error;
public:
    File_Not_Contain (const std::string& name_file, const std::string& string);
    std::string get_error () override;
};

class Text_Error : public Base_Error {
private:
    std::string text_error;
public:
    Text_Error ();
    std::string get_error () override;
};

class Text_Empty : public Text_Error {
private:
    std::string text_error;
public:
    Text_Empty ();
    std::string get_error () override;
};

class Parameter_Error : public Base_Error {
private:
    std::string text_error;
public:
    Parameter_Error ();
    std::string get_error () override;
};

class Parameter_Empty : public Parameter_Error {
private:
    std::string text_error;
public:
    Parameter_Empty ();
    std::string get_error () override;
};

class Queue_Error : public Base_Error {
private:
    std::string text_error;
public:
    explicit Queue_Error (int);
    std::string get_error () override;
};

#endif