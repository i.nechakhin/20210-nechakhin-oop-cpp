#include "Worker.h"

void Worker::work (std::vector <std::string>* text) {
    validate(text);
    do_work(text);
}
