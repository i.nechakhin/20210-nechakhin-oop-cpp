#ifndef TASK_2_SORT_H_
#define TASK_2_SORT_H_

#include <algorithm>

#include "Worker.h"

class Sort : public Worker {
private:
    std::vector <std::string> params;
public:
    explicit Sort (const std::vector <std::string>&);
    void validate (std::vector<std::string> *text) override;
    void do_work (std::vector <std::string>* text) override;
    BLOCK_TYPE get_type () override;
};

#endif