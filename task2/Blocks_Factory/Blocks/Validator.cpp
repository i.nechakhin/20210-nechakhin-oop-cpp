#include "Validator.h"

File_Error::File_Error () {
    text_error = "Error. Problem with file.\n";
}

std::string File_Error::get_error () {
    return text_error;
}

File_Not_Opened::File_Not_Opened (const std::string& name_file) {
    text_error = "Error. File " + name_file + " not opened.\n";
}

std::string File_Not_Opened::get_error () {
    return text_error;
}

File_Not_Contain::File_Not_Contain (const std::string& name_file, const std::string& string) {
    text_error = "Error. File " + name_file + " not contain " + string + ".\n";
}

std::string File_Not_Contain::get_error () {
    return text_error;
}

Text_Error::Text_Error () {
    text_error = "Error. Problem with text.\n";
}

std::string Text_Error::get_error () {
    return text_error;
}

Text_Empty::Text_Empty () {
    text_error = "Error. Text empty.\n";
}

std::string Text_Empty::get_error () {
    return text_error;
}

Parameter_Error::Parameter_Error () {
    text_error = "Error. Problem with parameter(s).\n";
}

std::string Parameter_Error::get_error () {
    return text_error;
}

Parameter_Empty::Parameter_Empty () {
    text_error = "Error. Parameters empty.\n";
}

std::string Parameter_Empty::get_error () {
    return text_error;
}

Queue_Error::Queue_Error (int parameter) {
    if (parameter == 0) {
        text_error = "Error. In the begin queue not IN block.\n";
    } else if (parameter == 1) {
        text_error = "Error. In the middle queue not INOUT block.\n";
    } else if (parameter == 2) {
        text_error = "Error. In the end queue not OUT block.\n";
    } else {
        text_error = "Error. Problem with queue.\n";
    }
}

std::string Queue_Error::get_error () {
    return text_error;
}
