#include "Grep.h"

Grep::Grep (const std::vector <std::string>& input_params) {
    params = input_params;
}

void Grep::validate (std::vector <std::string>* text) {
    if (params.empty()) {
        throw Parameter_Empty();
    }
    if (params[0].empty()) {
        throw Parameter_Error();
    }
    if (text->empty()) {
        throw Text_Empty();
    }
}

bool Grep::is_word_in_string (std::string word, std::string string) {
    if (string.empty()) {
        return false;
    }
    bool is_contain = false;
    size_t size_string = string.size();
    size_t size_word = word.size();
    for (int i = 0; i < size_string - size_word; ++ i) {
        if (string[i] == word[0]) {
            for (int j = 1; j < word.size(); ++ j) {
                if (string[i + j] != word[j]) {
                    break;
                }
                if (j == size_word - 1) {
                    is_contain = true;
                }
            }
        }
        if (is_contain) {
            break;
        }
    }
    return is_contain;
}

// params[0] == word
void Grep::do_work (std::vector <std::string>* text) {
    std::vector <std::string> separated_text;
    size_t size_text = text->size();
    for (int i = 0; i < size_text; ++ i) {
        if (is_word_in_string(params[0], (*text)[i])) {
            separated_text.push_back((*text)[i]);
        }
    }
    *text = separated_text;
}

BLOCK_TYPE Grep::get_type () {
    return BLOCK_TYPE::INOUT;
}