#include "Replace.h"

Replace::Replace (const std::vector <std::string>& input_params) {
    params = input_params;
}

void Replace::validate (std::vector <std::string>* text) {
    if (params.empty()) {
        throw Parameter_Empty();
    }
    if (params[0].empty()) {
        throw Parameter_Error();
    }
    if (text->empty()) {
        throw Text_Empty();
    }
}

void Replace::replace_word_in_string (std::string* input_string, const std::string& word1, const std::string& word2) {
    std::string result(*input_string);
    size_t pos = result.find(word1);
    while (pos != std::string::npos) {
        result.replace(pos, word1.size(), word2);
        pos = result.find(word1, pos);
    }
    *input_string = result;
}

// params[0] == word 1
// params[1] == word 2
void Replace::do_work (std::vector <std::string>* text) {
    size_t size_text = text->size();
    for (int i = 0; i < size_text; ++ i) {
        replace_word_in_string(&(*text)[i], params[0], params[1]);
    }
}

BLOCK_TYPE Replace::get_type () {
    return BLOCK_TYPE::INOUT;
}