#include "Writefile.h"

Writefile::Writefile (const std::vector <std::string>& input_params) {
    params = input_params;
}

void Writefile::validate (std::vector <std::string>* text) {
    if (params.empty()) {
        throw Parameter_Empty();
    }
    if (text->empty()) {
        throw Text_Empty();
    }
    std::ofstream output_file;
    output_file.open(params[0]);
    if (!output_file.is_open()) {
        throw File_Not_Opened(params[0]);
    }
    output_file.close();
}

// params[0] == filename
void Writefile::do_work (std::vector <std::string>* text) {
    std::ofstream output_file;
    output_file.open(params[0]);
    size_t size_text = text->size();
    for (int i = 0; i < size_text; ++ i) {
        output_file << (*text)[i] << '\n';
    }
    output_file.close();
}

BLOCK_TYPE Writefile::get_type () {
    return BLOCK_TYPE::OUT;
}