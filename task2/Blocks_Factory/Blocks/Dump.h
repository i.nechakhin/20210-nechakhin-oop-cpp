#ifndef TASK_2_DUMP_H_
#define TASK_2_DUMP_H_

#include <fstream>

#include "Worker.h"

class Dump : public Worker {
private:
    std::vector <std::string> params;
public:
    explicit Dump (const std::vector <std::string>&);
    void validate (std::vector <std::string>* text) override;
    void do_work (std::vector <std::string>* text) override;
    BLOCK_TYPE get_type () override;
};

#endif