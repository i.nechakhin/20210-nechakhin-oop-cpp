#ifndef TASK_2_REPLACE_H_
#define TASK_2_REPLACE_H_

#include "Worker.h"

class Replace : public Worker {
public:
    std::vector <std::string> params;
    static void replace_word_in_string (std::string*, const std::string&, const std::string&);
public:
    explicit Replace (const std::vector <std::string>&);
    void validate (std::vector<std::string> *text) override;
    void do_work (std::vector <std::string>* text) override;
    BLOCK_TYPE get_type () override;
};

#endif