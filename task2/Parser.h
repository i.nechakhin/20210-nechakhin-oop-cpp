#ifndef TASK_2_PARSER_H_
#define TASK_2_PARSER_H_

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "Blocks_Factory/Blocks/Validator.h"

typedef std::vector <std::pair <int, std::pair <std::string, std::vector <std::string>>>> vector_pair;

class Parser {
private:
    static std::vector <std::string> split_equal (const std::string&);
    static std::pair <std::string, std::vector <std::string>> split_space (const std::string&);
    static std::vector <int> split_queue (const std::string&);
    static bool is_file_contain (const std::string&, const std::string&);
public:
    void parse (const std::string&, vector_pair*, std::vector <int>*);
};

#endif