#include "Referee.h"

void Referee::set_ships (vector <Ship>& ships_first, vector <Ship>& ships_second) {
    for (Ship ship : ships_first) {
        ship.destroyed = false;
    }
    for (Ship ship : ships_second) {
        ship.destroyed = false;
    }
    this->ships1 = ships_first;
    this->ships2 = ships_second;
}

int Referee::find_ship_by_coord (int x, int y, bool first) {
    for (int i = 0; i < (first ? ships1 : ships2).size(); ++ i) {
        Ship ship = (first ? ships1 : ships2)[i];
        if (ship.horizontal && y == ship.y && ship.x + get_length(ship.type) - 1 >= x) {
            for (int j = 0; j < get_length(ship.type); ++ j) {
                if (ship.x + j == x) {
                    return i;
                }
            }
        } else if (!ship.horizontal && x == ship.x) {
            for (int j = 0; j < get_length(ship.type); ++ j) {
                if (ship.y + j == y) {
                    return i;
                }
            }
        }
    }
    return -1;
}

bool Referee::gamer_fire (bool first, vector <vector <int>>& field, const shared_ptr <Gamer>& gamer) {
    while (true) {
        pair <int, int> coord = gamer->next_step();
        if (coord.first < 0 || coord.first > 9 || coord.second < 0 || coord.second > 9) {
            gamer->success_destruction(false);
            gamer->success_fire(false);
            continue;
        }
        if (field[coord.second][coord.first] == EMPTY_FIELD) {
            field[coord.second][coord.first] = FIRED;
            gamer->success_destruction(false);
            gamer->success_fire(false);
            return false;
        } else if (field[coord.second][coord.first] == FIRED || field[coord.second][coord.first] == DAMAGED) {
            gamer->success_destruction(false);
            gamer->success_fire(false);
            return false;
        } else if (field[coord.second][coord.first] == PLAYER_SHIP) {
            int i = find_ship_by_coord(coord.first, coord.second, !first);
            gamer->success_destruction(false);
            gamer->success_fire(true);
            Ship& ship = (first ? ships2 : ships1)[i];
            if (ship.health <= 1) {
                gamer->success_destruction(true);
                ship.destroyed = true;
            }
            ship.health--;
            field[coord.second][coord.first] = DAMAGED;
            return true;
        }
    }
}

bool Referee::can_fill_ships (vector <vector <int>>& field, vector <Ship>& ships) {
    for (Ship ship : ships) {
        if (!is_good_place_for_ship(field, ship.type, ship.x, ship.y, ship.horizontal)) {
            return false;
        }
    }
    return true;
}

bool Referee::check_presence_all_ship (vector <Ship>& ships) {
    int count_battleship = 0;
    int count_cruiser = 0;
    int count_destroyer = 0;
    int count_torpedo_boat = 0;
    for (auto& ship : ships) {
        if (ship.type == BATTLESHIP) {
            count_battleship++;
        } else if (ship.type == CRUISER) {
            count_cruiser++;
        } else if (ship.type == DESTROYER) {
            count_destroyer++;
        } else if (ship.type == TORPEDO_BOAT) {
            count_torpedo_boat++;
        }
    }
    return count_battleship == 1 && count_cruiser == 2 && count_destroyer == 3 && count_torpedo_boat == 4;
}

bool Referee::gamer_is_dead (bool first) {
    if (first) {
        for (Ship ship : ships1) {
            if (!ship.destroyed) {
                return false;
            }
        }
    } else {
        for (Ship ship : ships2) {
            if (!ship.destroyed) {
                return false;
            }
        }
    }
    return true;
}