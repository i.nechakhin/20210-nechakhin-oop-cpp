#ifndef TASK3_REFEREE_H
#define TASK3_REFEREE_H

#include <memory>
#include <vector>

#include "Gamers/Gamer.h"

class Referee {
private:
    vector <Ship> ships1;
    vector <Ship> ships2;
    int find_ship_by_coord (int x, int y, bool first);
public:
    void set_ships (vector <Ship>& ships_first, vector <Ship>& ships_second);
    bool gamer_is_dead (bool first);
    bool gamer_fire (bool first, vector <vector <int>>& field, const shared_ptr <Gamer>& gamer);
    static bool can_fill_ships (vector <vector <int>>& field, vector <Ship>& ships);
    static bool check_presence_all_ship (vector <Ship>& ships);
};


#endif