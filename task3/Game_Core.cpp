#include "Game_Core.h"

pair <bool, string> Game_Core::reload_game (vector <vector <int>>& field_first, vector <vector <int>>& field_second,
                                            const shared_ptr <Gamer>& first, const shared_ptr <Gamer>& second, Referee& referee) {
    field_first.clear();
    field_second.clear();
    for (int i = 0; i < 10; ++ i) {
        vector <int> row(10, EMPTY_FIELD);
        field_first.push_back(row);
        field_second.push_back(row);
    }
    vector <Ship> vector_ships1(first->create_ships());
    vector <Ship> vector_ships2(second->create_ships());
    if (!Referee::check_presence_all_ship(vector_ships1) || !Referee::can_fill_ships(field_first, vector_ships1)) {
        return make_pair <bool, string>(false, "Bad ships set of first player!");
    }
    if (!Referee::check_presence_all_ship(vector_ships2) || !Referee::can_fill_ships(field_second, vector_ships2)) {
        return make_pair <bool, string>(false, "Bad ships set of second player!");
    }
    referee.set_ships(vector_ships1, vector_ships2);
    for (int i = 0; i < 10; ++ i) {
        set_ship(field_first, vector_ships1[i].type, vector_ships1[i].x, vector_ships1[i].y, vector_ships1[i].horizontal);
        set_ship(field_second, vector_ships2[i].type, vector_ships2[i].x, vector_ships2[i].y, vector_ships2[i].horizontal);
    }
    return make_pair(true, "");
}

void Game_Core::play_game (vector <vector <int>>& field_first, vector <vector <int>>& field_second,
                           const shared_ptr <Gamer>& first, const shared_ptr <Gamer>& second,
                           Referee& referee, int round, int score0, int score1) {
    shared_ptr <Game_View> game_view = make_shared <Console_View>();
    bool move = (rand() % 2);
    bool success_fire;
    while (!referee.gamer_is_dead(true) && !referee.gamer_is_dead(false)) {
        do {
            game_view->show_game(field_first, field_second, round, score0, score1);
            success_fire = referee.gamer_fire(move, move ? field_second : field_first, move ? first : second);
        } while (success_fire && !referee.gamer_is_dead(true) && !referee.gamer_is_dead(false));
        move = !move;
    }
}

shared_ptr <Gamer> Game_Core::create_gamer (Type_Gamer type_gamer) {
    shared_ptr <Gamer> gamer;
    if (type_gamer == CONSOLE) {
        gamer =  make_shared <Console_Gamer>();
    } else if (type_gamer == OPTIMAL) {
        gamer = make_shared <Optimal_Gamer>();
    } else if (type_gamer == RANDOM) {
        gamer = make_shared <Random_Gamer>();
    } else {
        gamer = nullptr;
    }
    return gamer;
}

void Game_Core::run (Type_Gamer first_type, Type_Gamer second_type, int rounds) {
    shared_ptr <Game_View> game_view = make_shared <Console_View>();
    shared_ptr <Gamer> first = create_gamer(first_type);
    shared_ptr <Gamer> second = create_gamer(second_type);
    vector <vector <int>> field_first;
    vector <vector <int>> field_second;
    Referee referee;
    int score_first = 0;
    int score_second = 0;
    for (int i = 0; i < rounds; ++ i) {
        pair <bool, string> res = reload_game(field_first, field_second, first, second, referee);
        if (!res.first) {
            cout << res.second << endl;
            first.reset();
            second.reset();
            return;
        }
        play_game(field_first, field_second, first, second, referee, i + 1, score_first, score_second);
        game_view->show_winner_in_round(referee.gamer_is_dead(false));
        score_first += referee.gamer_is_dead(false);
        score_second += referee.gamer_is_dead(true);
        game_view->show_game(field_first, field_second, i + 1, score_first, score_second);
    }
    if (score_first == score_second) {
        pair <bool, string> res = reload_game(field_first, field_second, first, second, referee);
        if (!res.first) {
            cout << res.second << endl;
            first.reset();
            second.reset();
            return;
        }
        play_game(field_first, field_second, first, second, referee, rounds + 1, score_first, score_second);
        score_first += referee.gamer_is_dead(false);
        score_second += referee.gamer_is_dead(true);
        rounds ++;
    }
    game_view->show_game(field_first, field_second, rounds, score_first, score_second);
    game_view->show_final_winner(score_first > score_second);
    first.reset();
    second.reset();
}

