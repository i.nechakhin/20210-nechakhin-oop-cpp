#ifndef TASK3_GAME_CORE_H
#define TASK3_GAME_CORE_H

#include <cstdlib>
#include <memory>
#include <vector>

#include "Gamers/Gamer.h"
#include "Gamers/Console_Gamer.h"
#include "Gamers/Optimal_Gamer.h"
#include "Gamers/Random_Gamer.h"
#include "Game_View.h"
#include "Console_View.h"
#include "Referee.h"

using namespace std;

enum Type_Gamer {
    CONSOLE,
    OPTIMAL,
    RANDOM,
};

class Game_Core {
private:
    static pair <bool, string> reload_game (vector <vector <int>>& field_first, vector <vector <int>>& field_second,
                                     const shared_ptr <Gamer>& first, const shared_ptr <Gamer>& second, Referee& referee);
    static void play_game (vector <vector <int>>& field_first, vector <vector <int>>& field_second,
                    const shared_ptr <Gamer>& first, const shared_ptr <Gamer>& second,
                    Referee& referee, int round, int score0, int score1);
    static shared_ptr <Gamer> create_gamer (Type_Gamer type_gamer);
public:
    static void run (Type_Gamer first_type, Type_Gamer second_type, int rounds);
};

#endif