#include <iostream>
#include "optionparser.h"

#include "Game_Core.h"

using namespace std;

enum optionIndex {
    UNKNOWN,
    HELP,
    FIRST,
    SECOND,
    COUNT,
};

const option::Descriptor usage[] = {
        {UNKNOWN, 0, "",  "",       option::Arg::Optional,  "USAGE: example_arg [options]\n\n"
                                                   "Options:"},
        {HELP,    0, "",  "help",   option::Arg::None,
         "  \t--help  \tYou can choose the type of two players and the number of rounds in the series."
         "\n\t\t\tTotal player types 3:"
         "\n\t\t\tRandom - absolutely random AI;"
         "\n\t\t\tOptimal - sharpened for victory AI;"
         "\n\t\t\tConsole - user takes control.\t"},
        {FIRST,   0, "f", "first",  option::Arg::Optional, "  -f[<arg>], \t--first[=<arg>]"
                                                   "  \tType of first player. Default is Random."},
        {SECOND,  0, "s", "second", option::Arg::Optional, "  -s[<arg>], \t--second[=<arg>]"
                                                   "  \tType of second player. Default is Random."},
        {COUNT,   0, "c", "count",  option::Arg::Optional,  "  -c <num>, \t--count=<num>  "
                                                   "\tNumber of rounds in a series. Default is 1."},
        {0,       0, 0,   0,        0,             0}
};

int main (int argc, char* argv[]) {
    // skip program name, argv[0] if present
    argc -= (argc > 0);
    argv += (argc > 0);
    option::Stats stats(usage, argc, argv);
    option::Option options[stats.options_max], buffer[stats.buffer_max];
    option::Parser parse(usage, argc, argv, options, buffer);
    if (parse.error()) {
        return 1;
    }
    if (options[HELP]) {
        option::printUsage(fwrite, stdout, usage);
        return 0;
    }
    if (options[UNKNOWN]) {
        return 0;
    }
    if (parse.nonOptionsCount() > 0) {
        for (int i = 0; i < parse.nonOptionsCount(); ++ i)
            cout << "Unknown argument " << i << " is " << parse.nonOption(i) << endl;
        return 0;
    }
    int count = 1;
    Type_Gamer first_type = RANDOM;
    Type_Gamer second_type = RANDOM;
    if (options[COUNT]) {
        count = (int) strtol(options[COUNT].arg, nullptr, 10);
        if (count <= 0) {
            cout << "Number of rounds must be positive!" << endl;
            return 0;
        }
    }
    if (options[FIRST]) {
        if (options[FIRST].arg == (string) "Console") {
            first_type = CONSOLE;
        } else if (options[FIRST].arg == (string) "Optimal") {
            first_type = OPTIMAL;
        } else if (options[FIRST].arg == (string) "Random") {
            first_type = RANDOM;
        } else {
            cout << "Unknown player type: " << options[FIRST].arg << "!" << endl;
            return 0;
        }
    }
    if (options[SECOND]) {
        if (options[SECOND].arg == (string) "Console") {
            second_type = CONSOLE;
        } else if (options[SECOND].arg == (string) "Optimal") {
            second_type = OPTIMAL;
        } else if (options[SECOND].arg == (string) "Random") {
            second_type = RANDOM;
        } else {
            cout << "Unknown player type: " << options[SECOND].arg << "!" << endl;
            return 0;
        }
    }
    Game_Core::run(first_type, second_type, count);
}