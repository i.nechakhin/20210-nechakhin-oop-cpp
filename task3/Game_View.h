#ifndef TASK3_GAME_VIEW_H
#define TASK3_GAME_VIEW_H

#include <vector>

using namespace std;

class Game_View {
public:
    virtual void show_game (vector <vector <int>> field1, vector <vector <int>> field2, int round, int score0, int score1) = 0;
    virtual void show_field_editor (vector <vector <int>> field0) = 0;
    virtual void show_winner_in_round (bool is_first) = 0;
    virtual void show_final_winner (bool is_first) = 0;
};

#endif