#ifndef TASK3_SHIP_H
#define TASK3_SHIP_H

#include <vector>

using namespace std;

enum Type_Cage {
    DAMAGED = -3,
    FIRED = -2,
    SURROUND = -1,
    EMPTY_FIELD = 0,
    PLAYER_SHIP = 1,
};

enum Type_Ship {
    BATTLESHIP = 0,
    CRUISER = 1,
    DESTROYER = 2,
    TORPEDO_BOAT = 3,
    UNKNOWN_SHIP,
};

int get_length (Type_Ship type);

typedef struct Ship {
    Type_Ship type;
    int x;
    int y;
    int health;
    bool horizontal;
    bool destroyed;
    Ship (Type_Ship type, int x, int y, bool horizontal);
} Ship;

void surround_ship (vector <vector <int>>& field, Ship ship);
bool is_good_place_for_ship (vector <vector <int>>& field, Type_Ship type, int x, int y, bool horizontal);
void set_ship (vector <vector <int>>& field, Type_Ship type, int x, int y, bool horizontal);

#endif