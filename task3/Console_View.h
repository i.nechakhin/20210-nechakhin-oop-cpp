#ifndef TASK3_CONSOLE_VIEW_H
#define TASK3_CONSOLE_VIEW_H

#include <iostream>
#include <vector>

#include "Gamers/Gamer.h"
#include "Game_View.h"

using namespace std;

class Console_View : public Game_View {
public:
    void show_game (vector <vector <int>> field1, vector <vector <int>> field2, int round, int score0, int score1) override;
    void show_field_editor (vector <vector <int>> field0) override;
    void show_winner_in_round (bool is_first) override;
    void show_final_winner (bool is_first) override;
};

#endif