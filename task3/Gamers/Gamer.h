#ifndef TASK3_GAMER_H
#define TASK3_GAMER_H

#include <vector>

#include "../Ship.h"

using namespace std;

class Gamer {
protected:
    bool success_fire_on_last = false;
    bool destroy_ship_on_last = false;
public:
    virtual void success_fire (bool success) = 0;
    virtual void success_destruction (bool success) = 0;
    virtual vector <Ship> create_ships () = 0;
    virtual pair <int, int> next_step () = 0;
};

#endif