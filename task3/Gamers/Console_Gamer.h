#ifndef TASK3_CONSOLE_GAMER_H
#define TASK3_CONSOLE_GAMER_H

#include <iostream>
#include <memory>

#include "Gamer.h"
#include "../Game_View.h"
#include "../Console_View.h"

class Console_Gamer : public Gamer {
private:
    int count_battleship = 0;
    int count_cruiser = 0;
    int count_destroyer = 0;
    int count_torpedo_boat = 0;
    void print_interface (vector <vector <int>>& field) const;
public:
    void success_destruction (bool success) override;
    void success_fire (bool success) override;
    vector <Ship> create_ships () override;
    pair <int, int> next_step () override;
};

#endif