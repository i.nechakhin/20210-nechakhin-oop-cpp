#ifndef TASK3_RANDOM_GAMER_H
#define TASK3_RANDOM_GAMER_H

#include <cstdlib>

#include "Gamer.h"

class Random_Gamer : public Gamer {
private:
    static void create_ship_one_type (vector <vector <int>>& field, vector <Ship>& ships, Type_Ship type_ship);
public:
    void success_destruction (bool success) override;
    void success_fire (bool success) override;
    vector <Ship> create_ships () override;
    pair <int, int> next_step () override;
};


#endif