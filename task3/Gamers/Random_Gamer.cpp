#include "Random_Gamer.h"

void Random_Gamer::success_destruction (bool success) {
    success_fire_on_last = success;
}

void Random_Gamer::success_fire (bool success) {
    destroy_ship_on_last = success;
}

void Random_Gamer::create_ship_one_type (vector <vector <int>>& field, vector <Ship>& ships, Type_Ship type_ship) {
    bool horizontal;
    int x, y;
    do {
        horizontal = rand() % 2;
        x = rand() % 10;
        y = rand() % 10;
    } while (!is_good_place_for_ship(field, type_ship, x, y, horizontal));
    Ship ship(type_ship, x, y, horizontal);
    ships.push_back(ship);
    surround_ship(field, ship);
    set_ship(field, type_ship, x, y, horizontal);
}

vector <Ship> Random_Gamer::create_ships () {
    vector <vector <int>> field;
    vector <Ship> ships;
    for (int i = 0; i < 10; ++ i) {
        vector <int> row(10, 0);
        field.push_back(row);
    }
    create_ship_one_type(field, ships, BATTLESHIP);
    for (int i = 0; i < 2; ++ i) {
        create_ship_one_type(field, ships, CRUISER);
    }
    for (int i = 0; i < 3; ++ i) {
        create_ship_one_type(field, ships, DESTROYER);
    }
    for (int i = 0; i < 4; ++ i) {
        create_ship_one_type(field, ships, TORPEDO_BOAT);
    }
    return ships;
}

pair <int, int> Random_Gamer::next_step () {
    int x = rand() % 10;
    int y = rand() % 10;
    return make_pair(x,y);
}
