#include "Optimal_Gamer.h"

void Optimal_Gamer::success_fire (bool success) {
    success_fire_on_last = success;
}

void Optimal_Gamer::success_destruction (bool success) {
    destroy_ship_on_last = success;
}

void Optimal_Gamer::create_vector_shots () {
    int index_x;
    int index_y;
    // optimal shots for search battleship
    index_x = 0;
    index_y = 3;
    for (int i = 0; i < 4; ++ i) {
        vector_shots.emplace_back(index_y, index_x);
        index_x ++;
        index_y --;
    }
    index_x = 0;
    index_y = 7;
    for (int i = 0; i < 8; ++ i) {
        vector_shots.emplace_back(index_y, index_x);
        index_x ++;
        index_y --;
    }
    index_x = 2;
    index_y = 9;
    for (int i = 0; i < 8; ++ i) {
        vector_shots.emplace_back(index_y, index_x);
        index_x ++;
        index_y --;
    }
    index_x = 6;
    index_y = 9;
    for (int i = 0; i < 4; ++ i) {
        vector_shots.emplace_back(index_y, index_x);
        index_x ++;
        index_y --;
    }
    // optimal shots for search cruiser
    index_x = 0;
    index_y = 1;
    for (int i = 0; i < 2; ++ i) {
        vector_shots.emplace_back(index_y, index_x);
        index_x ++;
        index_y --;
    }
    index_x = 0;
    index_y = 5;
    for (int i = 0; i < 6; ++ i) {
        vector_shots.emplace_back(index_y, index_x);
        index_x ++;
        index_y --;
    }
    index_x = 0;
    index_y = 9;
    for (int i = 0; i < 10; ++ i) {
        vector_shots.emplace_back(index_y, index_x);
        index_x ++;
        index_y --;
    }
    index_x = 4;
    index_y = 9;
    for (int i = 0; i < 6; ++ i) {
        vector_shots.emplace_back(index_y, index_x);
        index_x ++;
        index_y --;
    }
    index_x = 8;
    index_y = 9;
    for (int i = 0; i < 2; ++ i) {
        vector_shots.emplace_back(index_y, index_x);
        index_x ++;
        index_y --;
    }
    // all shots
    for (index_y = 0; index_y < 10; ++ index_y) {
        for (index_x = 0; index_x < 10; ++ index_x) {
            if (index_x % 2 == 1 && index_y % 2 == 1) {
                vector_shots.emplace_back(index_y, index_x);
            }
            if (index_x % 2 == 0 && index_y % 2 == 0) {
                vector_shots.emplace_back(index_y, index_x);
            }
        }
    }
}

vector <Ship> Optimal_Gamer::create_ships () {
    is_horizontal = 0;
    is_destroy_cycle = false;
    last_shots = make_pair(0, 0);
    last_success_shots.clear();
    buffer_shots.clear();
    vector_shots.clear();
    create_vector_shots();
    vector <Ship> ships;
    ships.emplace_back(BATTLESHIP, 0, 0, false);
    ships.emplace_back(CRUISER, 2, 0, false);
    ships.emplace_back(CRUISER, 2, 4, false);
    ships.emplace_back(DESTROYER, 0, 5, false);
    ships.emplace_back(DESTROYER, 0, 8, false);
    ships.emplace_back(DESTROYER, 2, 8, false);
    int index_y, index_x;
    index_y = rand() % 4;
    index_x = rand() % 2;
    ships.emplace_back(TORPEDO_BOAT, (index_x ? 5 : 4), index_y, true);
    index_y = rand() % 4;
    index_x = rand() % 2;
    ships.emplace_back(TORPEDO_BOAT, (index_x ? 9 : 8), index_y, true);
    index_y = rand() % 4;
    index_x = rand() % 2;
    ships.emplace_back(TORPEDO_BOAT, (index_x ? 5 : 4), 9 - index_y, true);
    index_y = rand() % 4;
    index_x = rand() % 2;
    ships.emplace_back(TORPEDO_BOAT, (index_x ? 9 : 8), 9 - index_y, true);
    return ships;
}

bool Optimal_Gamer::is_elem_vector_shots (pair <int, int> input_pair) {
    size_t size_vector = vector_shots.size();
    for (size_t i = 0; i < size_vector; ++ i) {
        if (vector_shots[i].first == input_pair.first && vector_shots[i].second == input_pair.second) {
            return true;
        }
    }
    return false;
}

void Optimal_Gamer::update_buffer_shots () {
    if (!buffer_shots.empty()) {
        buffer_shots.clear();
    }
    if (is_horizontal == 0 || is_horizontal < 0) {
        if (last_shots.first != 0 && is_elem_vector_shots(make_pair(last_shots.first - 1, last_shots.second))) {
            buffer_shots.emplace_back(last_shots.first - 1, last_shots.second);
        }
        if (last_shots.first != 9 && is_elem_vector_shots(make_pair(last_shots.first + 1, last_shots.second))) {
            buffer_shots.emplace_back(last_shots.first + 1, last_shots.second);
        }
    }
    if (is_horizontal == 0 || is_horizontal > 0) {
        if (last_shots.second != 0 && is_elem_vector_shots(make_pair(last_shots.first, last_shots.second - 1))) {
            buffer_shots.emplace_back(last_shots.first, last_shots.second - 1);
        }
        if (last_shots.second != 9 && is_elem_vector_shots(make_pair(last_shots.first, last_shots.second + 1))) {
            buffer_shots.emplace_back(last_shots.first, last_shots.second + 1);
        }
    }
}

void Optimal_Gamer::update_is_horizontal () {
    size_t size_buffer_shots = buffer_shots.size();
    if (size_buffer_shots == 3 || size_buffer_shots == 2) {
        is_horizontal = -1;
    }
    if (size_buffer_shots == 1 || size_buffer_shots == 0) {
        is_horizontal = 1;
    }
}

void Optimal_Gamer::remove_elem_vector_shots (pair <int, int> input_pair) {
    size_t size_vector = vector_shots.size();
    for (size_t i = 0; i < size_vector; ++ i) {
        if (vector_shots[i].first == input_pair.first && vector_shots[i].second == input_pair.second) {
            vector_shots.erase(vector_shots.begin() + i);
        }
    }
}

pair <int, int> Optimal_Gamer::next_step () {
    pair <int, int> next_shots;
    if (destroy_ship_on_last) {
        is_horizontal = 0;
        is_destroy_cycle = false;
        success_fire_on_last = false;
        destroy_ship_on_last = false;
        last_success_shots.clear();
        buffer_shots.clear();
    }
    if (!success_fire_on_last && !is_destroy_cycle) {
        next_shots = vector_shots.front();
    } else if (success_fire_on_last && !is_destroy_cycle) {
        is_destroy_cycle = true;
        last_success_shots.push_back(last_shots);
        update_buffer_shots();
        next_shots = buffer_shots.front();
    } else if (success_fire_on_last && is_destroy_cycle) {
        if (is_horizontal == 0) {
            update_is_horizontal();
        }
        last_success_shots.push_back(last_shots);
        size_t size_last_success_shots = last_success_shots.size();
        for (size_t i = 0; i < size_last_success_shots; ++ i) {
            update_buffer_shots();
            if (buffer_shots.empty()) {
                last_shots = last_success_shots[i];
            } else {
                break;
            }
        }
        next_shots = buffer_shots.front();
    } else if (!success_fire_on_last && is_destroy_cycle) {
        if (buffer_shots.empty()) {
            size_t size_last_success_shots = last_success_shots.size();
            for (size_t i = 0; i < size_last_success_shots; ++ i) {
                update_buffer_shots();
                if (buffer_shots.empty()) {
                    last_shots = last_success_shots[i];
                } else {
                    break;
                }
            }
        }
        next_shots = buffer_shots.front();
    }
    if (is_destroy_cycle) {
        buffer_shots.pop_front();
    }
    last_shots = next_shots;
    remove_elem_vector_shots(next_shots);
    return next_shots;
}
