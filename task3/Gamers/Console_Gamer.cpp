#include "Console_Gamer.h"

void Console_Gamer::success_destruction (bool success) {
    success_fire_on_last = success;
}

void Console_Gamer::success_fire (bool success) {
    destroy_ship_on_last = success;
}

void Console_Gamer::print_interface (vector <vector <int>>& field) const {
    shared_ptr <Game_View> console_view = make_shared <Console_View>();
    console_view->show_field_editor(field);
    cout << "Input <x(int)> <y(int)> <horizontal(y/n)> <Type_Ship(int)>" << endl;
    cout << "Type_Ship:" << endl;
    cout << "0 - battleship (size - 4, count - " << 1 - count_battleship << ")" << endl;
    cout << "1 - cruiser (size - 3, count - " << 2 - count_cruiser << ")" << endl;
    cout << "2 - destroyer (size - 2, count - " << 3 - count_destroyer << ")" << endl;
    cout << "3 - torpedo_boat (size - 1, count - " << 4 - count_torpedo_boat << ")" << endl;
}

vector <Ship> Console_Gamer::create_ships () {
    count_battleship = 0;
    count_cruiser = 0;
    count_destroyer = 0;
    count_torpedo_boat = 0;
    vector <vector <int>> field;
    vector <Ship> ships;
    for (int i = 0; i < 10; ++ i) {
        vector <int> row (10, EMPTY_FIELD);
        field.push_back(row);
    }
    print_interface(field);
    int x = 0, y = 0, type_ship = 0;
    char hor = 0;
    while (true) {
        cin >> x;
        if (!cin || !(x > 0 && x < 11)) {
            cout << "Wrong input, repeat please" << endl;
            continue;
        }
        cin >> y;
        if (!cin || !(y > 0 && y < 11)) {
            cout << "Wrong input, repeat please" << endl;
            continue;
        }
        cin >> hor;
        if (!cin || !(tolower(hor) == 'y' || tolower(hor) == 'n')) {
            cout << "Wrong input, repeat please" << endl;
            continue;
        }
        cin >> type_ship;
        if (!cin || !(type_ship > -1 && type_ship < 4)) {
            cout << "Wrong input, repeat please" << endl;
            continue;
        }
        Type_Ship type = UNKNOWN_SHIP;
        if (type_ship == BATTLESHIP) {
            if (count_battleship == 1) {
                cout << "You already have a battleship!" << endl;
                continue;
            }
            type = BATTLESHIP;
        } else if (type_ship == CRUISER) {
            if (count_cruiser == 2) {
                cout << "You already have a cruiser!" << endl;
                continue;
            }
            type = CRUISER;
        } else if (type_ship == DESTROYER) {
            if (count_destroyer == 3) {
                cout << "You already have a destroyer!" << endl;
                continue;
            }
            type = DESTROYER;
        } else if (type_ship == TORPEDO_BOAT) {
            if (count_torpedo_boat == 4) {
                cout << "You already have a torpedo_boat!" << endl;
                continue;
            }
            type = TORPEDO_BOAT;
        }
        if (is_good_place_for_ship(field, type, x - 1, y - 1, tolower(hor) == 'y')) {
            Ship ship(type, x - 1, y - 1, tolower(hor) == 'y');
            if (type == BATTLESHIP) {
                count_battleship ++;
            } else if (type == CRUISER) {
                count_cruiser ++;
            } else if (type == DESTROYER) {
                count_destroyer ++;
            } else if (type == TORPEDO_BOAT) {
                count_torpedo_boat ++;
            }
            ships.emplace_back(ship);
            surround_ship(field, ship);
            set_ship(field, type, x - 1, y - 1, tolower(hor) == 'y');
            print_interface(field);
        } else {
            cout << "Cannot be placed next to existing ships!" << endl;
            continue;
        }
        if (count_battleship == 1 && count_cruiser == 2 && count_destroyer == 3 && count_torpedo_boat == 4) {
            cout << "All ships are on display. To battle!" << endl;
            break;
        }
    }
    return ships;
}

pair <int, int> Console_Gamer::next_step () {
    cout << "Input <x(int)> <y(int)>" << endl;
    int x = 0, y = 0;
    while (true) {
        cin >> x;
        if (!cin || !(x > 0 && x < 11)) {
            cout << "Wrong coords, repeat please" << endl;
            continue;
        }
        cin >> y;
        if (!cin || !(y > 0 && y < 11)) {
            cout << "Wrong coords, repeat please" << endl;
            continue;
        }
        return make_pair(x - 1, y - 1);
    }
}
