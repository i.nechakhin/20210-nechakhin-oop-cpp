#ifndef TASK3_OPTIMAL_GAMER_H
#define TASK3_OPTIMAL_GAMER_H

#include <cstdlib>
#include <list>
#include <vector>

#include "Gamer.h"

class Optimal_Gamer : public Gamer {
private:
    int is_horizontal = 0;
    bool is_destroy_cycle = false;
    pair <int, int> last_shots;
    vector <pair <int, int>> last_success_shots;
    list <pair <int, int>> buffer_shots;
    vector <pair <int, int>> vector_shots;
    void create_vector_shots ();
    bool is_elem_vector_shots (pair <int, int>);
    void update_buffer_shots ();
    void update_is_horizontal ();
    void remove_elem_vector_shots (pair <int, int>);
public:
    void success_fire (bool success) override;
    void success_destruction (bool success) override;
    vector <Ship> create_ships () override;
    pair <int, int> next_step () override;
};


#endif