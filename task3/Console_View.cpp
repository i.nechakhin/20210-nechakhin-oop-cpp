#include "Console_View.h"

void Console_View::show_game (vector <vector <int>> field1, vector <vector <int>> field2, int round, int score0, int score1) {
    cout << "  1 2 3 4 5 6 7 8 9 10   1 2 3 4 5 6 7 8 9 10" << endl;
    for (int i = 0; i < 10; ++ i) {
        cout << (i + 1 == 10 ? to_string(i + 1) : to_string(i + 1) + " ");
        for (int j = 0; j < 10; ++ j) {
            if (field1[i][j] == PLAYER_SHIP) {
                cout << "#";
            } else if (field1[i][j] == EMPTY_FIELD || field1[i][j] == SURROUND) {
                cout << "~";
            } else if (field1[i][j] == DAMAGED) {
                cout << "X";
            } else if (field1[i][j] == FIRED) {
                cout << "*";
            }
            cout << " ";
        }
        cout << (i + 1 == 10 ? " " + to_string(i + 1) : " " + to_string(i + 1) + " ");
        for (int j = 0; j < 10; ++ j) {
            if (field2[i][j] == PLAYER_SHIP) {
                cout << "#";
            } else if (field2[i][j] == EMPTY_FIELD || field2[i][j] == SURROUND) {
                cout << "~";
            } else if (field2[i][j] == DAMAGED) {
                cout << "X";
            } else if (field2[i][j] == FIRED) {
                cout << "*";
            }
            cout << " ";
        }
        cout << endl;
    }
    cout << "       round: " + to_string(round) << endl;
    cout << "       score first player: " + to_string(score0) << endl;
    cout << "       score second player: " + to_string(score1) << endl;
}

void Console_View::show_field_editor (vector <vector <int>> field0) {
    cout << "  1 2 3 4 5 6 7 8 9 10" << endl;
    for (int i = 0; i < 10; ++ i) {
        cout << (i + 1 == 10 ? to_string(i + 1) : to_string(i + 1) + " ");
        for (int j = 0; j < 10; ++j) {
            if (field0[i][j] == PLAYER_SHIP) {
                cout << "#";
            } else if (field0[i][j] == EMPTY_FIELD || field0[i][j] == SURROUND) {
                cout << "~";
            } else if (field0[i][j] == DAMAGED) {
                cout << "X";
            } else if (field0[i][j] == FIRED) {
                cout << "*";
            }
            cout << " ";
        }
        cout << endl;
    }
}

void Console_View::show_winner_in_round (bool is_first) {
    if (is_first) {
        cout << "FIRST IS WINNER" << endl;
    } else {
        cout << "SECOND IS WINNER"<< endl;
    }
}

void Console_View::show_final_winner (bool is_first) {
    if (is_first) {
        cout << "FIRST IS FINAL WINNER" << endl;
    } else {
        cout << "SECOND IS FINAL WINNER"<< endl;
    }
}