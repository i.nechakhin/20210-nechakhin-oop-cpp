#include "Ship.h"

int get_length (Type_Ship type) {
    if (type == BATTLESHIP) {
        return 4;
    } else if (type == CRUISER) {
        return 3;
    } else if (type == DESTROYER) {
        return 2;
    } else if (type == TORPEDO_BOAT) {
        return 1;
    } else if (type == UNKNOWN_SHIP) {
        return 0;
    } else {
        return -1;
    }
}

Ship::Ship (Type_Ship type, int x, int y, bool horizontal) {
    this->type = type;
    this->x = x;
    this->y = y;
    this->horizontal = horizontal;
    this->destroyed = false;
    this->health = get_length(type);
}

void surround_ship (vector <vector <int>>& field, Ship ship) {
    int length = get_length(ship.type);
    int x = ship.x;
    int y = ship.y;
    for (int i = 0; i < length; ++ i) {
        if (x - 1 > -1 && field[y][x - 1] == EMPTY_FIELD) {
            field[y][x - 1] = SURROUND;
        }
        if (x + 1 < 10 && field[y][x + 1] == EMPTY_FIELD) {
            field[y][x + 1] = SURROUND;
        }
        if (x - 1 > -1 && y - 1 > -1 && field[y - 1][x - 1] == EMPTY_FIELD) {
            field[y - 1][x - 1] = SURROUND;
        }
        if (x - 1 > -1 && y + 1 < 10 && field[y + 1][x - 1] == EMPTY_FIELD) {
            field[y + 1][x - 1] = SURROUND;
        }
        if (x + 1 < 10 && y - 1 > -1 && field[y - 1][x + 1] == EMPTY_FIELD) {
            field[y - 1][x + 1] = SURROUND;
        }
        if (y + 1 < 10 && field[y + 1][x] == EMPTY_FIELD) {
            field[y + 1][x] = SURROUND;
        }
        if (y - 1 > -1 && field[y - 1][x] == EMPTY_FIELD) {
            field[y - 1][x] = SURROUND;
        }
        if (x + 1 < 10 && y + 1 < 10 && field[y + 1][x + 1] == EMPTY_FIELD) {
            field[y + 1][x + 1] = SURROUND;
        }
        if (ship.horizontal) {
            x ++;
        } else {
            y ++;
        }
    }
}

bool is_good_place_for_ship (vector <vector <int>>& field, Type_Ship type, int x, int y, bool horizontal) {
    int length = get_length(type);
    if (horizontal) {
        for (int i = 0; i < length; ++ i) {
            if ((x + i) > 9 || field[y][x + i] != EMPTY_FIELD) {
                return false;
            }
        }
    } else {
        for (int i = 0; i < length; ++ i) {
            if ((y + i) > 9 || field[y + i][x] != EMPTY_FIELD) {
                return false;
            }
        }
    }
    return true;
}

void set_ship (vector <vector <int>>& field, Type_Ship type, int x, int y, bool horizontal) {
    int length = get_length(type);
    if (horizontal) {
        for (int i = 0; i < length; ++ i) {
            field[y][x + i] = PLAYER_SHIP;
        }
    } else {
        for (int i = 0; i < length; ++ i) {
            field[y + i][x] = PLAYER_SHIP;
        }
    }
}