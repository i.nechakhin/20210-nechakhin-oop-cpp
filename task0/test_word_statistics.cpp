#include <gtest/gtest.h>
#include <iostream>

#include "word_statistics.h"

typedef std::pair<std::string, std::pair<int, double>> wrd_and_stat;

bool compare (const wrd_and_stat& first, const wrd_and_stat& second) {
    return (first.second.first > second.second.first);
}

bool comp_string (std::string string1, std::string string2) {
    size_t string1_size = string1.size();
    size_t string2_size = string2.size();
    if (string1_size == string2_size) {
        for (int i = 0; i < string1_size; ++ i) {
            if (string1[i] != string2[i]) {
                return false;
            }
        }
    }
    return true;
}

bool comp_wrd_and_stat (const wrd_and_stat& wrd_stat1, const wrd_and_stat& wrd_stat2) {
    if (!comp_string(wrd_stat1.first, wrd_stat2.first)) {
        return false;
    }
    if (wrd_stat1.second.first != wrd_stat2.second.first) {
        return false;
    }
    if (wrd_stat1.second.second != wrd_stat2.second.second) {
        return false;
    }
    return true;
}

::testing::AssertionResult list_word_match (std::list <wrd_and_stat> list_word1, std::list <wrd_and_stat> list_word2) {
    list_word1.sort(compare);
    list_word2.sort(compare);
    size_t list_word1_size = list_word1.size();
    size_t list_word2_size = list_word2.size();
    if (list_word1_size != list_word2_size) {
        return ::testing::AssertionFailure();
    }
    for (int i = 0; i < list_word2_size; ++ i) {
        if (!comp_wrd_and_stat(list_word1.front(), list_word2.front())) {
            return ::testing::AssertionFailure();
        }
        list_word1.pop_front();
        list_word2.pop_front();
    }
    return ::testing::AssertionSuccess();
}

void fill_in_word_statistics (word_statistics* word_statistic, std::list <std::string> list_words) {
    while (!list_words.empty()) {
        word_statistic->add(list_words.front());
        list_words.pop_front();
    }
}

TEST (word_statistics_test_consider_case, normal_nos) {
    auto word_statistic1 = new word_statistics(true);
    std::list <wrd_and_stat> list_word_statistic1;
    std::list <wrd_and_stat> answer1 = {\
    std::make_pair("Hello", std::make_pair(2, 50.0)), \
    std::make_pair("world", std::make_pair(2, 50.0))};
    std::list <std::string> list_words1 = {"Hello", "world", "Hello", "world"};
    fill_in_word_statistics(word_statistic1, list_words1);
    word_statistic1->convert_to_list(list_word_statistic1);
    EXPECT_TRUE(list_word_match(list_word_statistic1, answer1));
    ASSERT_EQ(word_statistic1->count_read_words(), 4);
    delete word_statistic1;

    auto word_statistic2 = new word_statistics(true);
    std::list <wrd_and_stat> list_word_statistic2;
    std::list <wrd_and_stat> answer2 = {\
    std::make_pair("Hello", std::make_pair(2, 50.0)), \
    std::make_pair("World", std::make_pair(1, 25.0)), \
    std::make_pair("world", std::make_pair(1, 25.0))};
    std::list <std::string> list_words2 = {"Hello", "Hello", "World", "world"};
    fill_in_word_statistics(word_statistic2, list_words2);
    word_statistic2->convert_to_list(list_word_statistic2);
    EXPECT_TRUE(list_word_match(list_word_statistic2, answer2));
    ASSERT_EQ(word_statistic1->count_read_words(), 4);
    delete word_statistic2;
}

TEST (word_statistics_test_not_consider_case, normal_nos) {
    auto word_statistic1 = new word_statistics(false);
    std::list <wrd_and_stat> list_word_statistic1;
    std::list <wrd_and_stat> answer1 = {\
    std::make_pair("hello", std::make_pair(2, 50.0)), \
    std::make_pair("world", std::make_pair(2, 50.0))};
    std::list <std::string> list_words1 = {"Hello", "World", "Hello", "world"};
    fill_in_word_statistics(word_statistic1, list_words1);
    word_statistic1->convert_to_list(list_word_statistic1);
    EXPECT_TRUE(list_word_match(list_word_statistic1, answer1));
    ASSERT_EQ(word_statistic1->count_read_words(), 4);
    delete word_statistic1;

    auto word_statistic2 = new word_statistics(false);
    std::list <wrd_and_stat> list_word_statistic2;
    std::list <std::string> list_words2 = {"Hello", "HELLO", "world", "wOrLd"};
    fill_in_word_statistics(word_statistic2, list_words2);
    word_statistic2->convert_to_list(list_word_statistic2);
    EXPECT_TRUE(list_word_match(list_word_statistic2, answer1));
    ASSERT_EQ(word_statistic1->count_read_words(), 4);
    delete word_statistic2;
}

TEST (word_statistics_test, not_normal_nos) {
    auto word_statistic1 = new word_statistics(true);
    std::list <wrd_and_stat> list_word_statistic1;
    std::list <wrd_and_stat> answer1 = {};
    std::list <std::string> list_words1 = {};
    fill_in_word_statistics(word_statistic1, list_words1);
    word_statistic1->convert_to_list(list_word_statistic1);
    EXPECT_TRUE(list_word_match(list_word_statistic1, answer1));
    ASSERT_EQ(word_statistic1->count_read_words(), 0);
    delete word_statistic1;

    auto word_statistic2 = new word_statistics(false);
    std::list <wrd_and_stat> list_word_statistic2;
    std::list <std::string> list_words2 = {};
    fill_in_word_statistics(word_statistic2, list_words2);
    word_statistic2->convert_to_list(list_word_statistic2);
    EXPECT_TRUE(list_word_match(list_word_statistic2, answer1));
    ASSERT_EQ(word_statistic1->count_read_words(), 0);
    delete word_statistic2;
}

int main (int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
