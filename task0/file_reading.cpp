#include "file_reading.h"

file_reading::file_reading (const std::string& input_path_to_file) {
    path_to_file = input_path_to_file;
}

void file_reading::open_file () {
    file.open(path_to_file);
}

bool file_reading::is_open () {
    return file.is_open();
}

std::string file_reading::get_line () {
    std::string line;
    if (file.eof()) {
        line = '\0';
    } else {
        std::getline(file, line);
    }
    return line;
}

bool file_reading::is_eof () {
    return file.eof();
}

void file_reading::close_file () {
    file.close();
}