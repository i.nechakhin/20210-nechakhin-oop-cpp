#ifndef TASK_0_SPLITTING_H_
#define TASK_0_SPLITTING_H_

#include <iostream>
#include <list>

class splitting {
    public:
        std::list <std::string> split (const std::string&);
    private:
        std::string line;
};

#endif