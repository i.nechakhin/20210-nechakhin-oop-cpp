#include <iostream>

#include "file_reading.h"
#include "file_writing.h"
#include "splitting.h"
#include "word_statistics.h"

typedef std::pair<std::string, std::pair<int, double>> wrd_and_stat;

bool compare (const wrd_and_stat& first, const wrd_and_stat& second) {
    return (first.second.first > second.second.first);
}

int main() {
    auto input = new file_reading("/home/ilya/CLionProjects/20210-nechakhin-oop-cpp/task0/input.txt");
    input->open_file();
    if (!input->is_open()) {
        delete input;
        return 1;
    }
    auto splitter = new splitting;
    auto word_statistic = new word_statistics(true);
    while (!input->is_eof()) {
        std::list <std::string> list_words = splitter->split(input->get_line());
        while (!list_words.empty()) {
            word_statistic->add(list_words.front());
            list_words.pop_front();
        }
    }
    input->close_file();
    delete input;
    delete splitter;
    std::list <std::pair<std::string, std::pair<int, double>>> list_word_statistic;
    word_statistic->convert_to_list(list_word_statistic);
    list_word_statistic.sort(compare);
    delete word_statistic;
    auto output = new file_writing("/home/ilya/CLionProjects/20210-nechakhin-oop-cpp/task0/output.csv");
    output->open_file();
    if (!output->is_open()) {
        delete output;
        return 1;
    }
    auto list_begin = list_word_statistic.begin();
    auto list_end = list_word_statistic.end();
    for (auto i = list_begin; i != list_end; ++ i) {
        std::string line;
        line = i->first + "," + std::to_string(i->second.first) + "," + std::to_string(i->second.second) + "%";
        output->write_line(line);
    }
    output->close_file();
    delete output;
    return 0;
}
