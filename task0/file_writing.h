#ifndef TASK_0_FILE_WRITING_H_
#define TASK_0_FILE_WRITING_H_

#include <fstream>
#include <iostream>
#include <list>

class file_writing {
    public:
        explicit file_writing (std::string);
        void open_file ();
        bool is_open ();
        void write_line (const std::string&);
        void close_file ();
    private:
        std::string path_to_file;
        std::ofstream file;
};

#endif