#include "word_statistics.h"

word_statistics::word_statistics (const bool& input_consider_case) {
    consider_case = input_consider_case;
    count_words = 0;
}

static void tolower_word (std::string& word) {
    size_t size_word = word.size();
    for (int i = 0; i < size_word; ++ i) {
        word[i] = tolower(word[i]);
    }
}

void word_statistics::add (std::string word) {
    if (!consider_case) {
        tolower_word(word);
    }
    if (!statistics_words.count(word)) {
        statistics_words[word] = 1;
    } else {
        statistics_words[word] += 1;
    }
    count_words ++;
}

void word_statistics::remove (std::string word) {
    if (!consider_case) {
        tolower_word(word);
    }
    statistics_words.erase(statistics_words.find(word));
    count_words --;
}

bool word_statistics::is_empty () {
    if (statistics_words.empty()) {
        return true;
    }
    return false;
}

size_t word_statistics::count_read_words () const {
    return count_words;
}

void word_statistics::convert_to_list (std::list <std::pair<std::string, std::pair<int, double>>>& list) {
    auto stat_begin = statistics_words.begin();
    auto stat_end = statistics_words.end();
    for (auto i = stat_begin; i != stat_end; ++ i) {
        std::pair<std::string, std::pair<int, double>> temp;
        temp.first = i->first;
        temp.second.first = statistics_words[i->first];
        temp.second.second =  ((double) statistics_words[i->first] / count_words) * 100;
        list.push_back(temp);
    }
}
