#ifndef TASK_0_FILE_READING_H_
#define TASK_0_FILE_READING_H_

#include <fstream>
#include <iostream>

class file_reading {
    public:
        explicit file_reading (const std::string&);
        void open_file ();
        bool is_open ();
        std::string get_line ();
        bool is_eof ();
        void close_file ();
    private:
        std::string path_to_file;
        std::ifstream file;
};

#endif