#include <gtest/gtest.h>
#include <iostream>

#include "splitting.h"

bool comp_string (std::string string1, std::string string2) {
    size_t string1_size = string1.size();
    size_t string2_size = string2.size();
    if (string1_size == string2_size) {
        for (int i = 0; i < string1_size; ++ i) {
            if (string1[i] != string2[i]) {
                return false;
            }
        }
    }
    return true;
}

::testing::AssertionResult list_match (std::list <std::string> list1, std::list <std::string> list2) {
    size_t list1_size = list1.size();
    size_t list2_size = list2.size();
    if (list1_size != list2_size) {
        return ::testing::AssertionFailure();
    }
    for (int i = 0; i < list2_size; ++ i) {
        if (!comp_string(list1.front(), list2.front())) {
            return ::testing::AssertionFailure();
        }
        list1.pop_front();
        list2.pop_front();
    }
    return ::testing::AssertionSuccess();
}

TEST (splitting_test, normal_nos) {
    auto splitter = new splitting;
    std::list <std::string> answer1 = {"Petya", "walked", "along", "the", "pavement"};
    EXPECT_TRUE(list_match(splitter->split("Petya walked along the pavement"), answer1));
    EXPECT_TRUE(list_match(splitter->split("Petya/walked@along-the+pavement"), answer1));
    std::list <std::string> answer2 = {"Pety1a", "w4lked", "2along4", "the", "p4vemen1", "123"};
    EXPECT_TRUE(list_match(splitter->split("Pety1a w4lked 2along4 the p4vemen1 123"), answer2));
    std::list <std::string> answer3 = {"123", "45", "67", "8", "9"};
    EXPECT_TRUE(list_match(splitter->split("123 45 67 8 9"), answer3));
    EXPECT_TRUE(list_match(splitter->split("123/45(67)8=9"), answer3));
    delete splitter;
}

TEST (splitting_test, not_normal_nos) {
    auto splitter = new splitting;
    std::list <std::string> answer1 = {};
    EXPECT_TRUE(list_match(splitter->split(""), answer1));
    std::list <std::string> answer2 = {"Petya", "walked", "along", "the", "pavement"};
    EXPECT_TRUE(list_match(splitter->split("  Petya  walked  along  the  pavement   "), answer2));
    delete splitter;
}

int main (int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}