#include "file_writing.h"

file_writing::file_writing (std::string output_path_to_file) {
    path_to_file = output_path_to_file;
}

void file_writing::open_file () {
    file.open(path_to_file);
}

bool file_writing::is_open () {
    return file.is_open();
}

void file_writing::write_line (const std::string& line) {
    file << line << std::endl;
}

void file_writing::close_file () {
    file.close();
}
