#include "splitting.h"

static bool is_number_or_letter (char symbol) {
    if (('a' <= symbol && symbol <= 'z') || ('A' <= symbol && symbol <= 'Z') || ('0' <= symbol && symbol <= '9')) {
        return true;
    }
    return false;
}

std::list <std::string> splitting::split (const std::string& input_line) {
    line = input_line;
    std::list <std::string> list_word;
    int line_size = line.size();
    std::string temp_line;
    for (int i = 0; i < line_size; ++ i) {
        if (is_number_or_letter(line[i])) {
            temp_line += line[i];
        } else {
            if (!temp_line.empty()) {
                list_word.push_back(temp_line);
            }
            temp_line = "";
        }
    }
    if (!temp_line.empty()) {
        list_word.push_back(temp_line);
    }
    return list_word;
}
