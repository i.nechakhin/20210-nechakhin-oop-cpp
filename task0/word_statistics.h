#ifndef TASK_0_WORD_STATISTICS_H_
#define TASK_0_WORD_STATISTICS_H_

#include <iostream>
#include <list>
#include <map>

class word_statistics {
    public:
        explicit word_statistics (const bool&);
        void add (std::string);
        void remove (std::string);
        bool is_empty ();
        [[nodiscard]] size_t count_read_words () const;
        void convert_to_list (std::list <std::pair<std::string, std::pair<int, double>>>&);
    private:
        std::map <std::string, int> statistics_words;
        bool consider_case;
        size_t count_words;
};

#endif