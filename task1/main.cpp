#include <iostream>

#include "Trit_set.h"

int main() {
    Tritset set(20, Trit(TRUE));
    set[2] = UNKNOWN;
    set[3] = FALSE;
    set[4] |= set[2];
    set[5] &= set[2];
    set[6] |= set[3];
    set[7] &= set[3];
    set[8] = set[4] | set[5];
    set[9] = set[4] & set[5];
    set[10] = set[2] | set[3];
    set[11] = set[2] & set[3];
    //set[12] = Trit(FALSE) | set[13];
    std::cout << set.to_string() << '\n';

    Tritset set1(5);
    std::cout << set1.to_string() << '\n';

    Tritset set2(20, Trit(TRUE));
    std::cout << set2.to_string() << '\n';

    set1[1] = TRUE;
    set2[2] = FALSE;
    Tritset set3 = set1 | set2;
    std::cout << set3.to_string() << '\n';

    Tritset set4 = set1 & set2;
    std::cout << set4.to_string() << '\n';

    set4.shrink();
    std::cout << set4.to_string() << '\n';

    Tritset set5 = ~set3;
    std::cout << set5.to_string() << '\n';
    return 0;
}

