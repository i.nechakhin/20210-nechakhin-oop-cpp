#ifndef TASK_1_TRIT_SET_H_
#define TASK_1_TRIT_SET_H_

#include <cstddef>
#include <string>
#include <vector>

#include "Trit.h"

class Tritset {
private:
    static const unsigned int trits_per_element;
    static unsigned int realize_fill_value (trit val);
    std::vector <unsigned int> array;
    size_t index_last;
    void update_index_last ();
    static Trit get_value (const Tritset&, int);
    static void set_value (Tritset&, int, const Trit&);
public:
    Tritset();
    explicit Tritset(size_t, const Trit& = Trit(UNKNOWN));

    class reference {
    private:
        Tritset* set_tritset;
        int index;
        Trit get_value_reference () const;
        void set_value_reference (const Trit&);
    public:
        reference (Tritset*, size_t);
        reference& operator &= (const Trit&);
        reference& operator &= (const reference&);
        reference& operator |= (const Trit&);
        reference& operator |= (const reference&);
        reference& operator = (const Trit&);
        reference& operator = (const reference&);
        Trit operator ! () const;
        Trit operator & (const Trit&) const;
        Trit operator & (const reference&) const;
        Trit operator | (const Trit&) const;
        Trit operator | (const reference&) const;
        bool operator == (const Trit&) const;
        bool operator == (const reference&) const;
        bool operator != (const Trit&) const;
        bool operator != (const reference&) const;
    };

    Tritset& operator &= (const Tritset&);
    Tritset& operator |= (const Tritset&);
    Tritset operator ~ () const;
    Tritset operator & (const Tritset&) const;
    Tritset operator | (const Tritset&) const;

    size_t capacity () const;
    size_t length() const;
    size_t shrink();
    void increase (size_t);

    reference operator [] (int);
    Trit operator [] (int) const;

    std::string to_string() const;
};

#endif