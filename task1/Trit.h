#ifndef TASK_1_TRIT_H_
#define TASK_1_TRIT_H_

enum trit {
    TRUE,
    UNKNOWN,
    FALSE
};

class Trit {
private:
    trit value_trit;
public:
    Trit ();
    Trit (trit);
    Trit (const Trit&);
    Trit& operator &= (const Trit&);
    Trit& operator |= (const Trit&);
    Trit& operator = (const Trit&);
    Trit operator ! () const;
    Trit operator & (const Trit&) const;
    Trit operator | (const Trit&) const;
    bool operator == (const Trit&) const;
    bool operator != (const Trit&) const;
    bool operator == (const trit&) const;
    bool operator != (const trit&) const;
    trit value () const;
};

#endif