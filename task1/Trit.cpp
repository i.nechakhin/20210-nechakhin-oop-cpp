#include "Trit.h"

Trit::Trit () : value_trit(UNKNOWN) {}

Trit::Trit (const trit value) : value_trit(value) {}

Trit::Trit (const Trit& obj) : value_trit(obj.value_trit) {}

Trit& Trit::operator &= (const Trit& other) {
    if (value_trit == FALSE || other.value_trit == FALSE) {
        value_trit = FALSE;
    } else if (value_trit == UNKNOWN || other.value_trit == UNKNOWN) {
        value_trit = UNKNOWN;
    } else {
        value_trit = TRUE;
    }
    return (*this);
}

Trit& Trit::operator |= (const Trit& other) {
    if (value_trit == TRUE || other.value_trit == TRUE) {
        value_trit = TRUE;
    } else if (value_trit == UNKNOWN || other.value_trit == UNKNOWN) {
        value_trit = UNKNOWN;
    } else {
        value_trit = FALSE;
    }
    return (*this);
}

Trit& Trit::operator = (const Trit& other) {
    if ((*this) == other) {
        return (*this);
    }
    value_trit = other.value_trit;
    return (*this);
}

Trit Trit::operator ! () const {
    if (value_trit == FALSE) {
        return Trit(TRUE);
    } else if (value_trit == TRUE) {
        return Trit(FALSE);
    }
    return Trit(UNKNOWN);
}

Trit Trit::operator & (const Trit& other) const {
    return Trit(*this) &= other;
}

Trit Trit::operator | (const Trit& other) const {
    return Trit(*this) |= other;
}

bool Trit::operator == (const Trit& other) const {
    return value_trit == other.value_trit;
}

bool Trit::operator != (const Trit& other) const {
    return value_trit != other.value_trit;
}

bool Trit::operator == (const trit& other) const {
    return value_trit == other;
}

bool Trit::operator != (const trit& other) const {
    return value_trit != other;
}

trit Trit::value () const {
    return value_trit;
}
