#include <gtest/gtest.h>

#include "../Trit_set.h"

TEST(test_tritset, constructors) {
    Tritset set(20, Trit(TRUE));
    ASSERT_EQ(set.to_string(), "11111111111111111111????????????");
    Tritset set2(10, Trit(FALSE));
    ASSERT_EQ(set2.to_string(), "0000000000??????");
    Tritset set3(10);
    ASSERT_EQ(set3.to_string(), "????????????????");
}

TEST(test_tritset, length) {
    ASSERT_EQ(Tritset(20, Trit(TRUE)).length(), 20);
    ASSERT_EQ(Tritset(10, Trit(FALSE)).length(), 10);
    ASSERT_EQ(Tritset(10).length(), 0);
}

TEST(test_tritset, capacity) {
    ASSERT_EQ(Tritset(20, Trit(TRUE)).capacity(), 32);
    ASSERT_EQ(Tritset(10, Trit(FALSE)).capacity(), 16);
    ASSERT_EQ(Tritset(10).capacity(), 16);
}
/*
TEST (test_tritset, check_shrink) {
    Tritset set1(40);
    set1[2] = Trit(TRUE);
    set1.shrink();
    ASSERT_EQ(set1.capacity(), 16);
    Tritset set2(40);
    set2[20] = Trit(TRUE);
    set2.shrink();
    ASSERT_EQ(set2.capacity(), 32);
}
*/
TEST(test_tritset, increase) {
    Tritset set(40, Trit(TRUE));
    set.increase(60);
    ASSERT_EQ(set.capacity(), 64);
    ASSERT_EQ(set.length(), 40);
    set.increase(20);
    ASSERT_EQ(set.capacity(), 64);
    ASSERT_EQ(set.length(), 40);
}

TEST(test_tritset, reference) {
    Tritset set(20, Trit(TRUE));
    set[2] = Trit(UNKNOWN);
    set[3] = Trit(FALSE);
    set[4] |= set[2];
    set[5] &= set[2];
    set[6] |= set[3];
    set[7] &= set[3];
    set[8] = set[4] | set[5];
    set[9] = set[4] & set[5];
    set[10] = set[2] | set[3];
    set[11] = set[2] & set[3];
    set[12] = !set[12];
    set[14] = set[15] & Trit(UNKNOWN);
    ASSERT_EQ(set.to_string(), "11?01?101??001?11111????????????");
    ASSERT_TRUE(set[0] == set[1]);
    ASSERT_TRUE(set[0] != set[2]);
    ASSERT_TRUE(set[2] != set[3]);
}

TEST(test_tritset, operarors) {
    Tritset set1(5);
    Tritset set2(20, Trit(TRUE));
    set1[1] = TRUE;
    set2[2] = FALSE;
    Tritset set3 = set1 | set2;
    ASSERT_EQ(set3.to_string(), "11?11111111111111111????????????");
    ASSERT_EQ(set3.capacity(), set2.capacity());
    Tritset set4 = set1 & set2;
    ASSERT_EQ(set4.to_string(), "?10?????????????????????????????");
    ASSERT_EQ(set4.capacity(), set2.capacity());
    Tritset set5 = ~set3;
    ASSERT_EQ(set5.to_string(), "00?00000000000000000????????????");
    ASSERT_EQ(set5.capacity(), set3.capacity());
}

int main (int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}