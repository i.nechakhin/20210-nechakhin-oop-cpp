#include <gtest/gtest.h>
#include <iostream>

#include "../Trit.h"

TEST(test_trit, create) {
    ASSERT_EQ(Trit().value(), UNKNOWN);
    ASSERT_EQ(Trit(TRUE).value(), TRUE);
    Trit temp = Trit(FALSE);
    ASSERT_EQ(Trit(temp).value(), FALSE);
}

TEST(test_trit, compare_operators) {
    Trit t = Trit(TRUE);
    Trit f = Trit(FALSE);
    Trit u = Trit(UNKNOWN);
    ASSERT_TRUE(t == t);
    ASSERT_TRUE(f == f);
    ASSERT_TRUE(u == u);
    ASSERT_TRUE(f != t);
    ASSERT_TRUE(t != u);
    ASSERT_TRUE(u != t);
}

TEST(test_trit, logical_and) {
    Trit t = Trit(TRUE);
    Trit f = Trit(FALSE);
    Trit u = Trit(UNKNOWN);
    ASSERT_EQ(f & f, f);
    ASSERT_EQ(u & u, u);
    ASSERT_EQ(t & t, t);
    ASSERT_EQ(t & u, u);
    ASSERT_EQ(t & f, f);
    ASSERT_EQ(f & u, f);
    ASSERT_EQ(u & f, f);
}

TEST(test_trit, logical_or) {
    Trit t = Trit(TRUE);
    Trit f = Trit(FALSE);
    Trit u = Trit(UNKNOWN);
    ASSERT_EQ(f | f, f);
    ASSERT_EQ(u | u, u);
    ASSERT_EQ(t | t, t);
    ASSERT_EQ(t | u, t);
    ASSERT_EQ(t | f, t);
    ASSERT_EQ(f | u, u);
    ASSERT_EQ(u | f, u);
}

TEST(test_trit, logilan_not) {
    Trit t = Trit(TRUE);
    Trit f = Trit(FALSE);
    Trit u = Trit(UNKNOWN);
    ASSERT_EQ(!t, f);
    ASSERT_EQ(!f, t);
    ASSERT_EQ(!u, u);
}

int main (int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}