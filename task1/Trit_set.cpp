#include "Trit_set.h"

const unsigned int Tritset::trits_per_element(sizeof(unsigned int) * 8 / 2);

Tritset::Tritset () {
    array.resize(0);
    index_last = -1;
}

unsigned int Tritset::realize_fill_value (trit val) {
    unsigned int elem;
    if (val == FALSE) {
        elem = 0b00u;
    } else if (val == TRUE) {
        elem = 0b11u;
    } else {
        elem = 0b01u;
    }
    auto res = (unsigned int) 0;
    for (unsigned int i = 0; i < trits_per_element; ++ i) {
        res |= (elem << (i * 2));
    }
    return res;
}

Tritset::Tritset (size_t size, const Trit& fill_value) {
    index_last = size - 1;
    size_t needed = (size / trits_per_element) + ((size % trits_per_element) ? 1 : 0);
    bool flag = (size % trits_per_element) > 0;
    if (fill_value.value() == FALSE) {
        array.resize(needed, realize_fill_value(FALSE));
    } else if (fill_value.value() == TRUE) {
        array.resize(needed, realize_fill_value(TRUE));
    } else {
        array.resize(needed, realize_fill_value(UNKNOWN));
        index_last = -1;
    }
    if (flag && (fill_value.value() != UNKNOWN)) {
        for (size_t i = size; i < needed * trits_per_element; ++ i) {
            (*this)[i] = UNKNOWN;
        }
    }
}

Trit Tritset::get_value (const Tritset& set_tritset, int index) {
    if (index >= set_tritset.capacity()) {
        return Trit(UNKNOWN);
    }
    unsigned int value = set_tritset.array[index / Tritset::trits_per_element];
    value >>= (index % Tritset::trits_per_element) * 2;
    value &= 0b11u;
    if (value == 0b00u) {
        return Trit(FALSE);
    } else if (value == 0b11u) {
        return Trit(TRUE);
    } else {
        return Trit(UNKNOWN);
    }
}

void Tritset::set_value (Tritset& set_tritset, int index, const Trit& value) {
    if (index >= set_tritset.capacity()) {
        if (value == UNKNOWN) {
            return;
        }
        set_tritset.increase(index + 1);
        set_tritset.index_last = index;
    }
    size_t id = index / Tritset::trits_per_element;
    size_t shift = (index % Tritset::trits_per_element) * 2;
    set_tritset.array[id] &= ~(0b11u << shift);
    if (value == TRUE) {
        set_tritset.array[id] |= (0b11u << shift);
    } else if (value == UNKNOWN) {
        set_tritset.array[id] |= (0b01u << shift);
    } else {
        set_tritset.array[id] |= (0b00u << shift);
    }
}

Trit Tritset::reference::get_value_reference () const {
    return get_value(*set_tritset, index);
}

void Tritset::reference::set_value_reference (const Trit& value) {
    set_value(*set_tritset, index, value);
}

Tritset::reference::reference (Tritset* new_set, size_t new_index) : set_tritset(new_set), index(new_index) {
    if (new_set->capacity() > new_index) {
        Trit(get_value_reference());
    }
}

Tritset::reference& Tritset::reference::operator &= (const Trit& other) {
    set_value_reference(get_value_reference() & other);
    return (*this);
}

Tritset::reference& Tritset::reference::operator &= (const Tritset::reference& other) {
    set_value_reference(get_value_reference() & other.get_value_reference());
    return (*this);
}

Tritset::reference& Tritset::reference::operator |= (const Trit& other) {
    set_value_reference(get_value_reference() | other);
    return (*this);
}

Tritset::reference& Tritset::reference::operator |= (const Tritset::reference& other) {
    set_value_reference(get_value_reference() | other.get_value_reference());
    return (*this);
}

Tritset::reference& Tritset::reference::operator = (const Trit& other) {
    set_value_reference(other);
    return (*this);
}

Tritset::reference& Tritset::reference::operator = (const Tritset::reference& other) {
    if (this == &other) {
        return (*this);
    }
    return (*this) = other.get_value_reference();
}

Trit Tritset::reference::operator ! () const {
    return !get_value_reference();
}

Trit Tritset::reference::operator & (const Trit& other) const {
    return get_value_reference() &= other;
}

Trit Tritset::reference::operator & (const Tritset::reference& other) const {
    return get_value_reference() &= other.get_value_reference();
}

Trit Tritset::reference::operator | (const Trit& other) const {
    return get_value_reference() |= other;
}

Trit Tritset::reference::operator | (const Tritset::reference& other) const {
    return get_value_reference() |= other.get_value_reference();
}

bool Tritset::reference::operator == (const Trit& other) const {
    return other == get_value_reference();
}

bool Tritset::reference::operator == (const Tritset::reference& other) const {
    return other.get_value_reference() == get_value_reference();
}

bool Tritset::reference::operator != (const Trit& other) const {
    return other != get_value_reference();
}

bool Tritset::reference::operator != (const Tritset::reference& other) const {
    return other.get_value_reference() != get_value_reference();
}

void Tritset::update_index_last () {
    index_last = -1;
    size_t array_size = capacity();
    for (int i = 0; i < array_size; ++ i) {
        if ((*this)[i] != UNKNOWN)
            index_last = i;
    }
}

Tritset& Tritset::operator &= (const Tritset& other) {
    size_t len_min, len_max;
    if (other.length() > length()) {
        len_max = other.length();
        increase(len_max);
        for (int i = 0; i < len_max; ++ i) {
            (*this)[i] &= Trit(other[i]);
        }
    } else {
        len_max = length();
        len_min = other.length();
        for (int i = 0; i < len_min; ++ i) {
            (*this)[i] &= Trit(other[i]);
        }
        for (size_t i = len_min; i < len_max; ++ i) {
            (*this)[i] &= Trit(UNKNOWN);
        }
    }
    update_index_last();
    return (*this);
}

Tritset& Tritset::operator |= (const Tritset& other) {
    size_t len_min, len_max;
    if (other.length() > length()) {
        len_max = other.length();
        increase(len_max);
        for (int i = 0; i < len_max; ++ i) {
            (*this)[i] |= Trit(other[i]);
        }
    } else {
        len_max = length();
        len_min = other.length();
        for (int i = 0; i < len_min; ++ i) {
            (*this)[i] |= Trit(other[i]);
        }
        for (size_t i = len_min; i < len_max; ++ i) {
            (*this)[i] |= Trit(UNKNOWN);
        }
    }
    update_index_last();
    return (*this);
}

Tritset Tritset::operator ~ () const {
    Tritset copy(*this);
    for (int i = 0; i <= index_last; ++ i) {
        (copy[i]) = !(copy[i]);
    }
    copy.update_index_last();
    return copy;
}

Tritset Tritset::operator & (const Tritset& other) const {
    return Tritset(*this) &= other;
}

Tritset Tritset::operator | (const Tritset& other) const {
    return Tritset(*this) |= other;
}

Tritset::reference Tritset::operator [] (int index) {
    return Tritset::reference(this, index);
}

Trit Tritset::operator [] (int index) const {
    return get_value((*this), index);
}

size_t Tritset::capacity () const {
    return array.size() * trits_per_element;
}

size_t Tritset::length () const {
    return index_last + 1;
}

size_t Tritset::shrink () {
    if (index_last < 0) {
        array.resize(0);
        index_last = -1;
        return 0;
    }
    size_t needed = index_last / trits_per_element + ((index_last % trits_per_element) ? 1 : 0);
    array.resize(needed);
    for (size_t i = index_last + 1; i < capacity(); ++ i) {
        (*this)[i] = UNKNOWN;
    }
    update_index_last();
    return needed;
}

void Tritset::increase (size_t new_size) {
    size_t needed = (new_size / trits_per_element) + ((new_size % trits_per_element) ? 1 : 0);
    if (needed > array.size()) {
        array.resize(needed, realize_fill_value(UNKNOWN));
    }
}

std::string Tritset::to_string () const {
    std::string str;
    size_t len = capacity();
    str.resize(len);
    for (int i = 0; i < len; ++ i) {
        Trit t = (*this)[i];
        if (t == UNKNOWN) {
            str[i] = '?';
        } else if (t == TRUE) {
            str[i] = '1';
        } else {
            str[i] = '0';
        }
    }
    return str;
}

